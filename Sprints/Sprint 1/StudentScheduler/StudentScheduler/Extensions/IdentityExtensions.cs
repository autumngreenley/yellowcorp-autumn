﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace StudentScheduler.Extensions
{
    public static class IdentityExtensions
    {
        // Generic Add/Update - can be used when claims first get added, initially populated, or updated
        public static void AddUpdateClaim(this IPrincipal currentPrincipal, string key, string value)
        {
            var identity = currentPrincipal.Identity as ClaimsIdentity;
            if (identity == null)
                return;

            // check for existing claim and remove it
            var existingClaim = identity.FindFirst(key);
            if (existingClaim != null)
                identity.RemoveClaim(existingClaim);

            // add new claim
            identity.AddClaim(new Claim(key, value));
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties() { IsPersistent = true });
        }

        // Generic Retrieval - used when custom Get (below) does not exist or is not necessary
        public static string GetClaimValue(this IIdentity identity, string key)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst(key);
            // Test for null to avoid issues during local testing
            return (claim != null) ? claim.Value : string.Empty;
        }
        
        public static string GetUserFullName(this IIdentity identity)
        {
            return GetClaimValue(identity, "UserFullName");
        }

        public static string GetUserLogo(this IIdentity identity)
        {
            return GetClaimValue(identity, "UserLogo");
        }


        public static int GetSchoolId(this IIdentity identity)
        {
            int.TryParse(GetClaimValue(identity, "SchoolId"), out int i);
            return i;
        }

        public static string GetSchoolName(this IIdentity identity)
        {
            return GetClaimValue(identity, "SchoolName");
        }

        public static int GetMajorId(this IIdentity identity)
        {
            int.TryParse(GetClaimValue(identity, "GetMajorId"), out int i);
            return i;
        }

        public static string GetGoogleCalendarId(this IIdentity identity)
        {
            return GetClaimValue(identity, "GoogleCalendarId");
        }

    }
}