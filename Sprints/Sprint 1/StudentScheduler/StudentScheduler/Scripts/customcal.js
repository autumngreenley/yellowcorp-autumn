$(window).on("load",function () {
    var eventsArray = $("#dataDiv").data('arg1');
    console.log(eventsArray);
    

    $('#fullcalendar').fullCalendar({
        defaultView: 'agendaWeek',
        timeZone: 'local',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        defaultDate: new Date(),
        events: eventsArray
        /*events: [
            {
                title: 'Class???',
                dow: [1],
                start: '01:00',
                end: '05:00'
            },
            {
                title: 'Game',
                dow: [3],
                start: '15:00',
                end: '17:00'
            }
        ]*/
    });
});



