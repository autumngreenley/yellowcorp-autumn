﻿/*  
    ============================================================================================
    Script to alert user if Caps Lock is on when entering password.
    ============================================================================================
    Implementaiton Notes:

        1.  Add the following two lines after a password input element field. If you have 
            multiple password fields (Current, New, Confirm New), then place it at the bottom 
            of the whole form or somewhere that is visible when entering any of the fields:

            <!------------ Cap Locks Warning - Use capsLockWarning.js with this page ----------->
            <p id="pwdCapsWarning" style="color:red; display:none;">WARNING! Caps lock is ON.</p>
            <!---------------------------------------------------------------------------------->
        2.  Add the reference to this javascript to your page. That's it!

            <script type="text/javascript" src="~/Scripts/capsLockWarning.js"></script>

    ============================================================================================ 
*/

document.addEventListener("keyup", function (event) {
    // Get the warning texts 
    var pwdMsg = document.getElementById("pwdCapsWarning");

    // If "caps lock" is pressed, display the warning text
    if (event.getModifierState("CapsLock")) {
        pwdMsg.style.display = "block";
    } else {
        pwdMsg.style.display = "none"
    }
});



