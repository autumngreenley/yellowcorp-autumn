﻿$(document).ready(function () {
    schoolDropdownEnable();
});

$("#Roles").change(function () {
    schoolDropdownEnable();
});

function schoolDropdownEnable() {
    //var selectedVal = $("#SchoolId").find(':selected').val();
    var selectedText = $("#Roles").find(':selected').text();
    if ((selectedText == "Administrator") || (selectedText == "Select" )) {
        $("#SchoolId").val(null);
        $("#SchoolId").prop("disabled", true);
    }
    else {
        $("#SchoolId").prop("disabled", false);
    }
};