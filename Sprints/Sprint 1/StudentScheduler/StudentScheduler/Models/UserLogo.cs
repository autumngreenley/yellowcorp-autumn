﻿namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserLogos")]
    public partial class UserLogo
    {
        public int ID { get; set; }

        [Required]
        [StringLength(400)]
        [DisplayName("Link To Image")]
        public string Link { get; set; }

    }
}