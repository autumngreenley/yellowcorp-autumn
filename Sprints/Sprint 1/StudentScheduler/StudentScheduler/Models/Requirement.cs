namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Requirement
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Requirement()
        {
            MajorRequirements = new HashSet<MajorRequirement>();
            UserMajorRequirements = new HashSet<UserMajorRequirement>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Requirement")]
        public string RequirementTitle { get; set; }

        [Required]
        [StringLength(400)]
        [DisplayName("Requirement Description")]
        public string RequirementDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MajorRequirement> MajorRequirements { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserMajorRequirement> UserMajorRequirements { get; set; }
    }
}
