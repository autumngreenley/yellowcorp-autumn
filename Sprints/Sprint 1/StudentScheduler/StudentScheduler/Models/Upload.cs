﻿using System.Web;
using System.ComponentModel.DataAnnotations;

namespace StudentScheduler.Models
{
    public class Upload
    {
        [Required(ErrorMessage = "Please select file")]
        public HttpPostedFileBase file { get; set; }
    }
}