namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Courses")]
    public partial class Cours
    {
        public int ID { get; set; }

        public int MajorID { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Course")]
        public string CourseTitle { get; set; }

        [Required]
        [StringLength(400)]
        [DisplayName("Subtext")]
        public string CourseSubtext { get; set; }

        [Required]
        [StringLength(10)]
        [DisplayName("CRN")]
        public string CourseNumber { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Days of the Week")]
        public string CourseDays { get; set; }

        [DisplayName("Start Time")]
        public TimeSpan CourseStartTime { get; set; }

        [DisplayName("End Time")]
        public TimeSpan CourseEndTime { get; set; }

        [DisplayName("Credits")]
        public int CreditCount { get; set; }

        [StringLength(400)]
        [DisplayName("Attributes")]
        public string CourseAttributes { get; set; }

        [StringLength(400)]
        [DisplayName("Instructor(s)")]
        public string CourseInstructor { get; set; }

        [StringLength(100)]
        [DisplayName("Class Room")]
        public string CourseRoom { get; set; }

        public virtual Major Major { get; set; }

    }
    /* Code needed for later
     * public class DayOfWeek
    {
        public string Weekday { get; set; }
    }

    public class CoursDs
    {
        public Cours thisCours { get; set; }
        public List <DayOfWeek> CDays { get; set; }
    }
    */
}
