namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Major
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Major()
        {
            Courses = new HashSet<Cours>();
            MajorRequirements = new HashSet<MajorRequirement>();
			//Requirements = new HashSet<Requirement>();
            //RequirementRules = new HashSet<RequirementRule>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Major Name")]
        public string Title { get; set; }

        [StringLength(200)]
        [DisplayName("URL Link")]
        public string Href { get; set; }

        [DisplayName("School Name")]
        public int SchoolID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cours> Courses { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Requirement> Requirements { get; set; }

        [DisplayName("Requirements")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MajorRequirement> MajorRequirements { get; set; }

        public virtual School School { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<RequirementRule> RequirementRules { get; set; }
    }
}
