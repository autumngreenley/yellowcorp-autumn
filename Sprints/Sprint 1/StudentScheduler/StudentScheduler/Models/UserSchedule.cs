namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserSchedule
    {
        [Key]
        [DisplayName("Schedule ID")]
        public int ID { get; set; }

        [StringLength(256)]
        [DisplayName("User ID")]
        public string UserID { get; set; }
        
        [Required]
        [DisplayName("Activity Title")]
        [StringLength(100)]
        public string ActivityTitle { get; set; }

        [Required]
        [StringLength(400)]
        [DisplayName("Activity Description")]
        public string ActivityDescription { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Activity Days")]
        public string ActivityDays { get; set; }

        [DisplayName("Start Time")]
        public TimeSpan ActivityStartTime { get; set; }

        [DisplayName("End Time")]
        public TimeSpan ActivityEndTime { get; set; }
        
        [ForeignKey("UserID")]
        [DisplayName("User ID")]
        public virtual AspNetUser AspNetUser { get; set; }

        [DisplayName("Course ID")]
        public int? ActivityCourseID { get; set; }

        [DisplayName("Event ID on Google Calendar")]
        public string GoogleCourseID { get; set; }
    }
}
