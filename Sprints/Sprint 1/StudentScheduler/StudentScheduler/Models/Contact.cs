﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StudentScheduler.Models
{

    [Table("Contacts")]
    public class Contact
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Contact Name")]
        public string ContactName { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Contact Email")]
        public string ContactEmail { get; set; }

        [Required]
        [StringLength(1000)]
        [DisplayName("Contact Message")]
        public string ContactMessage { get; set; }
    }
}