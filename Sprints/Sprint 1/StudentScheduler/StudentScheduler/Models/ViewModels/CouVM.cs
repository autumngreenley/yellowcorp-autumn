﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentScheduler.Models.ViewModels
{
    public class CouVM
    {

            public string cName { get; set; }
            public string cSubtexts { get; set; }
            public string cCRN { get; set; }
            public string cAtt { get; set; }
            //public string cLocatio { get; set; } // dont need
            public int cCredits { get; set; }
            public string cTime { get; set; }
            public string cDays { get; set; }
            public string cClassrrom { get; set; }
            public string cInstructor { get; set; }
            public TimeSpan cSTimes { get; set; }
            public TimeSpan cETimes { get; set; }


        //0,1,2,3,5,6,7,8,10
            public List<Completion> CouList { get; set; }

    }
}