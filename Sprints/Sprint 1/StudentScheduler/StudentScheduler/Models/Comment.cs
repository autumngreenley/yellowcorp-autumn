﻿namespace StudentScheduler.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Comments")]
    public partial class Comment
    {
        public int ID { get; set; }


        [Required]
        [StringLength(200)]
        [DisplayName("Full Name")]
        public string FullName { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Email Address")]
        public string Email { get; set; }

        [Required]
        [StringLength(200)]
        [DisplayName("Subject")]
        public string FullSubject { get; set; }

        [Required]
        [StringLength(400)]
        [DisplayName("Message")]
        public string FullMessage { get; set; }
    }
}
