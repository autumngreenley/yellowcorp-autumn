﻿using StudentScheduler.DAL.Repositories.Abstract;

namespace StudentScheduler.DAL.Repositories.Concrete
{
    public class UnitOfWork : IUnitOfWork
    {
        private SchedulerContext db = new SchedulerContext();

        public UnitOfWork()
        {
            Majors = new MajorRepository(db);
            Schools = new SchoolRepository(db);
            UniversityEvents = new UniversityEventRepository(db);
        }

        public IMajorRepository Majors { get; private set; }
        public ISchoolRepository Schools { get; private set; }
        public IUniversityEventRepository UniversityEvents { get; private set; }

        public int Save()
        {
            return db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}