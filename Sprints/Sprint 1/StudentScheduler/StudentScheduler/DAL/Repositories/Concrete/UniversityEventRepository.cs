﻿using System.Collections.Generic;
using System.Linq;
using StudentScheduler.DAL.Repositories.Concrete.Patterns;
using StudentScheduler.DAL.Repositories.Abstract;
using StudentScheduler.Models;

namespace StudentScheduler.DAL.Repositories.Concrete
{
    public class UniversityEventRepository : Repository<UniversityEvent>, IUniversityEventRepository
    {
        public UniversityEventRepository(SchedulerContext context)
            : base(context)
        {
        }

        private SchedulerContext SchedulerContext
        {
            get { return Context as SchedulerContext;  }
        }

        // Specialized Methods Below:

        public IEnumerable<UniversityEvent> GetUniversityEventsForSchool(int schoolId)
        {
            return SchedulerContext.UniversityEvents.Where(m => m.SchoolID == schoolId).ToList();
        }
    }
}