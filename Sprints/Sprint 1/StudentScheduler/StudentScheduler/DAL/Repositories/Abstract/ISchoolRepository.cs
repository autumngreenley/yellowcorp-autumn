﻿using StudentScheduler.DAL.Repositories.Abstract.Patterns;
using StudentScheduler.Models;

namespace StudentScheduler.DAL.Repositories.Abstract
{
    public interface ISchoolRepository : IRepository<School>
    {   
    }
}
