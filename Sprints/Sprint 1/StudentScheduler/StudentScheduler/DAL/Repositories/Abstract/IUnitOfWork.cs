using StudentScheduler.DAL.Repositories.Abstract;
using System;

namespace StudentScheduler.DAL.Repositories.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        IMajorRepository Majors { get; }
        ISchoolRepository Schools { get; }
        IUniversityEventRepository UniversityEvents { get; }
        int Save();
    }
}