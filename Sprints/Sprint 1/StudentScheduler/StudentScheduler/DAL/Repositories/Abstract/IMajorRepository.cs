﻿using System.Collections.Generic;
using StudentScheduler.DAL.Repositories.Abstract.Patterns;
using StudentScheduler.Models;

namespace StudentScheduler.DAL.Repositories.Abstract
{
    public interface IMajorRepository : IRepository<Major>
    {
        IEnumerable<Major> GetMajorsForSchool(int schoolId);
    }
}
