﻿using System.Collections.Generic;
using StudentScheduler.DAL.Repositories.Abstract.Patterns;
using StudentScheduler.Models;

namespace StudentScheduler.DAL.Repositories.Abstract
{
    public interface IUniversityEventRepository : IRepository<UniversityEvent>
    {
        IEnumerable<UniversityEvent> GetUniversityEventsForSchool(int schoolId);
    }
}
