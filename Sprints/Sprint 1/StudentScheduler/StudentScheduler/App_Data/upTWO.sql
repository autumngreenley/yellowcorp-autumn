Drop TABLE	UserLogos
GO

CREATE TABLE UserLogos
(
	ID							INT IDENTITY(1,1) NOT NULL,
	Link						NVARCHAR(400),
	CONSTRAINT PK_UserLogos_ID	PRIMARY KEY CLUSTERED (ID)

);
GO

INSERT dbo.UserLogos
(
	Link
)
VALUES
('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5AeoWZKee3824PbXpUax1ykFuYMntWsmgjLII-VZ8JLk38MWL4Q'),
('https://media.istockphoto.com/vectors/funny-happy-cute-happy-smiling-avocado-vector-id939768622?k=6&m=939768622&s=612x612&w=0&h=BevVyOpXKWlyoceMuvXAqZnjZZfl_7i0EpOPQSTo8Ig='),
('https://i.pinimg.com/originals/29/21/a2/2921a2c69e186f704f87435027ba4987.jpg'),
('https://i.pinimg.com/originals/55/31/a4/5531a4d7815f295b475a1cb46ea46960.png'),
('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT96T2n_43ofcl4I0at2Z7W_qwMMXpTqyBFLcgZIg5-dW6Cu2eG8g'),
('http://www.amisvegetarian.com/wp-content/uploads/2018/09/ice-cream-images-clip-art-ice-cream-clip-art-ice-cream-images-animations.png'),
('https://www.culinaryschools.org/clipart/rubber-duck.png'),
('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR71iJ9EV6796UzbdZyJ9XrI1OSja4gcMf86CavaBJGc4PZGXtXxg')
GO

/*
CREATE Table UserLogos
(
	ID							INT IDENTITY(1,1) NOT NULL,
	UserID						NVARCHAR(128)		NOT NULL,
	Link						NVARCHAR(400),
	CONSTRAINT PK_UserLogos_ID	PRIMARY KEY CLUSTERED (ID)
);
GO

ALTER TABLE [dbo].[UserLogos]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserLogos_dbo.AspNetUsers_Id] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserLogos] CHECK CONSTRAINT [FK_dbo.UserLogos_dbo.AspNetUsers_Id]
GO
*/
/*
DROP TABLE Completions
GO

CREATE TABLE Completions
(
	ID							INT IDENTITY(1,1) NOT NULL,
	UserID						NVARCHAR(128)		NOT NULL,
	Requirements				VARCHAR(200)	NOT NULL,
	Subtexts					VARCHAR(100)	NOT NULL,
	CreditsRequired				INT				NOT NULL,
	CreditsCompleted			INT				NOT NULL,
	ClassesTaken				VARCHAR(200)	NOT NULL,
	CONSTRAINT PK_Completions_ID	PRIMARY KEY CLUSTERED (ID)
);
GO

ALTER TABLE [dbo].[Completions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Completions_dbo.AspNetUsers_Id] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Completions] CHECK CONSTRAINT [FK_dbo.Completions_dbo.AspNetUsers_Id]
GO
*/
/*
Use Scheduler
GO

CREATE TABLE Comments
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	FullName	 				VARCHAR(200)		NOT NULL,
	Email		 				VARCHAR(100)		NOT NULL,
	FullSubject					VARCHAR(200)		NOT NULL,
	FullMessage					VARCHAR(400)		NOT NULL,
	CONSTRAINT PK_Comments_ID	PRIMARY KEY CLUSTERED (ID)
);
GO
*/