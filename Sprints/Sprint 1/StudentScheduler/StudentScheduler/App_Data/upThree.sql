CREATE TABLE Schools
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	Title	 					VARCHAR(100)		NOT NULL,
	CONSTRAINT PK_Schools_ID	PRIMARY KEY CLUSTERED (ID)
);
GO

CREATE TABLE Majors
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	Title	 					VARCHAR(100)		NOT NULL,
	Href						VARCHAR(200)		NOT NULL,
	SchoolID					INT					NOT NULL,
	CONSTRAINT PK_Majors_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_Majors_ID		FOREIGN KEY (SchoolID)	
		REFERENCES Schools(ID)				
);
GO

CREATE TABLE Courses
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	MajorID						INT					NOT NULL,
	CourseTitle					VARCHAR(100)		NOT NULL,
	CourseSubtext				VARCHAR(400)		NOT NULL, --EDIT
	CourseNumber				VARCHAR(10)			NOT NULL,
	CourseDays					VARCHAR(100)		NOT NULL, --I still need to figure out how this would work for classes of multiple days
	CourseStartTime				TIME				NOT NULL,
	CourseEndTime				TIME				NOT NULL,
	CreditCount					INT					NOT NULL,
	CourseAttributes			VARCHAR(400)		NOT NULL, --NEW
	CourseInstructor			VARCHAR(400)		NOT NULL, --NEW
	CourseRoom					VARCHAR(100)		NOT NULL
	CONSTRAINT PK_CourseRequirements_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_CourseRequirements_ID		FOREIGN KEY (MajorID)	
		REFERENCES Majors(ID)
);
GO

CREATE TABLE Requirements
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	MajorID						INT					NOT NULL,
	RequirementTitle			VARCHAR(100)		NOT NULL,
	RequirementDescription		VARCHAR(400)		NOT NULL
	CONSTRAINT PK_Courses_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_Courses_ID		FOREIGN KEY (MajorID)	
		REFERENCES Majors(ID)
);
GO

CREATE TABLE RequirementRules
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	MajorID						INT					NOT NULL,
	RuleDescription				VARCHAR(400)		NOT NULL,
	RuleString					VARCHAR(50)			NOT NULL
	CONSTRAINT PK_RequirementRules_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_RequirementRules_ID		FOREIGN KEY (MajorID)	
		REFERENCES Majors(ID)
);
GO

CREATE TABLE UserSchedules
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	UserID						NVARCHAR(128)		NOT NULL,
	ActivityTitle				VARCHAR(100)		NOT NULL,
	ActivityDescription			VARCHAR(400)		NOT NULL,
	ActivityDays				VARCHAR(100)		NOT NULL, --I still need to figure out how this would work for activities of multiple days
	ActivityStartTime			TIME				NOT NULL,
	ActivityEndTime				TIME				NOT NULL,
	ActivityCourseID			INT					NULL,
	GoogleCourseID				VARCHAR(400)		NULL,
	CONSTRAINT PK_UserSchedules_ID		PRIMARY KEY CLUSTERED (ID)
);
GO

CREATE TABLE UniversityEvents
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	SchoolID					INT					NOT NULL,
	EventTitle					VARCHAR(100)		NOT NULL,
	EventDescription			VARCHAR(400)		NOT NULL,
	EventDays					VARCHAR(100)		NOT NULL, --I still need to figure out how this would work for activities of multiple days
	EventStartTime				TIME				NOT NULL,
	EventEndTime				TIME				NOT NULL
	CONSTRAINT PK_UniversityEvents_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_UniversityEvents_ID		FOREIGN KEY (SchoolID)	
		REFERENCES Schools(ID)
);
GO
/***
This table is for recording a users progress, after they upload
their degree progress
***/
CREATE TABLE Completions
(
	ID							INT IDENTITY(1,1) NOT NULL,
	Requirements				VARCHAR(200)	NOT NULL,
	Subtexts					VARCHAR(100)	NOT NULL,
	CreditsRequired				INT				NOT NULL,
	CreditsCompleted			INT				NOT NULL,
	ClassesTaken				VARCHAR(200)	NOT NULL,
	CONSTRAINT PK_Completions_ID	PRIMARY KEY CLUSTERED (ID)
);
GO

/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 3/4/2019 8:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[UserFullName] [nvarchar](128) NULL,
	[GoogleCalendarId] [nvarchar](256) NULL,
	[SchoolId] [int] NULL,
	[MajorId] [int] NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUsers_dbo.Schools_ID] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[Schools] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_dbo.AspNetUsers_dbo.Schools_ID]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUsers_dbo.Majors_ID] FOREIGN KEY([MajorId])
REFERENCES [dbo].[Majors] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_dbo.AspNetUsers_dbo.Majors_ID]
GO

ALTER TABLE [dbo].[UserSchedules]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserSchedules_dbo.AspNetUsers_Id] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserSchedules] CHECK CONSTRAINT [FK_dbo.UserSchedules_dbo.AspNetUsers_Id]
GO

INSERT dbo.Schools
(
	Title
)
VALUES
('Western Oregon University'),
('Oregon State University'),
('University of Oregon')
GO

INSERT dbo.Majors
(
	Title,
	Href,
	SchoolID
)
VALUES
/*('Computer Science', 1),
('Information Systems', 1),
('Visual Communications Design', 1),*/
('Computer Science','', 2)
GO

INSERT dbo.UniversityEvents
(
	SchoolID,
	EventTitle,
	EventDescription,
	EventDays,
	EventStartTime,
	EventEndTime
)
VALUES
(1, 'WOU Coffee Talk', 'Network with professionals in the field: ask questions, gain insight and learn more about your field.', 'March 12, 2019', '14:00:00', '16:00:00'),
(1, 'Smith Fine Art Series', 'Experience The Hot Club of San Fransico as it presents Cinema Vivant, an evening of silent films accompanied by live gypsy swing.','April 6, 2019', '16:30:00', '18:00:00'),
(1, 'Pow Wow', 'Experience Native American Culture with a ceremony involving dancing, singing, and feasting.', 'April 27, 2019', '12:00:00', '18:00:00'),
(1, 'WOU AES 2019', 'An entire day dedicated to the presentation of student scholarly activities.', 'May 30, 2019', '08:00:00', '16:00:00'),
(2, 'Spring Career Expo', 'Networking opportunity to meet prospect employers for expectant college graduates.', 'April 24, 2019', '11:00:00', '18:00:00'),
(3, 'The Illusionist', 'Experience hilarious magic trick, death-defying stunts, and acts of breathtaking wonder done by five of the most talented illusionists.', 'April 3, 2019', '12:00:00', '14:30:00')
GO

INSERT [dbo].[AspNetRoles] ([Id], [Name]) 
VALUES	(N'4a61e15a-5d32-4411-9e4f-8476baac9e4b', N'Student'),
		(N'6c5eaaba-7779-454a-a513-a8a83dfe8e41', N'Staff'),
		(N'7041742a-291b-49b1-8a5b-5909edec5c07', N'Administrator')
GO

INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [UserFullName], [GoogleCalendarId],[SchoolId],[MajorId]) 
VALUES	(N'5118604e-9f46-414a-acdc-a00102db3150', N'student@test.com', 0, N'AChSRQ8obtlXN1PQfDlAjkl8JBtRwjNOgXCxxDTiF1IFSgCsoEW7aOOzuwApvYe4uw==', N'0199c446-dac4-4cbd-90bc-b218c78dbed6', NULL, 0, 0, NULL, 1, 0, N'student@test.com', 'Joe Studential', NULL, 1, NULL),
		(N'88032915-db2a-4221-8878-2b571dc83c4f', N'staff@test.com', 0, N'AFG/qyukddXDQvRs/nYewOchEn5xMgAfjmXrBXymAlRAk0gmjWJayZhpVwOKBhI5CQ==', N'cc53347c-d72c-4a11-94ee-5d84f5a7b824', NULL, 0, 0, NULL, 1, 0, N'staff@test.com', 'Ann Staffer', NULL, 1, NULL),
		(N'edf1d47a-24bc-479c-b3af-2842e0d342ea', N'administrator@test.com', 0, N'AMqVUOtXxVaurtimzI7Dkp/D+zOm2E7IR8M4djMkAoZPvBcp1lEDrHM7MIogud1oyg==', N'e341a338-b9a2-4374-9d7c-87517278b03a', NULL, 0, 0, NULL, 1, 0, N'administrator@test.com', 'Elizabeth Adminieweather', NULL, NULL, NULL)
		GO

INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) 
VALUES	(N'5118604e-9f46-414a-acdc-a00102db3150', N'4a61e15a-5d32-4411-9e4f-8476baac9e4b'),
		(N'88032915-db2a-4221-8878-2b571dc83c4f', N'6c5eaaba-7779-454a-a513-a8a83dfe8e41'),
		(N'edf1d47a-24bc-479c-b3af-2842e0d342ea', N'7041742a-291b-49b1-8a5b-5909edec5c07')
GO
