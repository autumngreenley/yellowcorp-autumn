USE Scheduler
GO

CREATE TABLE Requirements
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	RequirementTitle			VARCHAR(100)		NOT NULL,
	RequirementDescription		VARCHAR(400)		NOT NULL
	CONSTRAINT PK_Requirements_ID		PRIMARY KEY CLUSTERED (ID)
);
GO

CREATE TABLE MajorRequirements
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	MajorID						INT					NOT NULL,
	ReqID						INT					NOT NULL
	CONSTRAINT PK_MajorRequirements_ID		PRIMARY KEY CLUSTERED (ID),
	CONSTRAINT FK_MajorRequirements_ID		FOREIGN KEY (MajorID)	
		REFERENCES Majors(ID),
	CONSTRAINT FK_MajorRequirements2_ID		FOREIGN KEY (ReqID)	
		REFERENCES Requirements(ID)
);
GO

CREATE TABLE UserMajorRequirements
(
	ID 							INT IDENTITY(1,1)	NOT NULL,
	UserID						NVARCHAR(128)		NOT NULL,
	ReqID						INT					NOT NULL,
	ReqMet						BIT					NOT NULL
	CONSTRAINT PK_UserMajorRequirements_ID	PRIMARY KEY CLUSTERED (ID)
);
GO

INSERT [dbo].[MajorRequirements] ([MajorID], [ReqID])
VALUES
		(1, 1),
		(1, 2),
		(1, 3),
		(1, 4),
		(1, 5),
		(1, 6),
		(1, 7),
		(1, 8),
		(1, 9),
		(1, 10),
		(1, 11),
		(1, 12),
		(1, 13),
		(1, 14),
		(1, 15),
		(1, 16),
		(1, 17),
		(1, 18),
		(1, 19),
		(1, 20),
		(1, 21),
		(1, 22),
		(1, 23),
		(1, 24),
		(1, 25),
		(1, 26),
		(1, 27),
		(1, 28),
		(2, 29),
		(2, 30),
		(2, 31),
		(2, 32),
		(2, 33)
GO

INSERT [dbo].[UserMajorRequirements] ([UserID], [ReqID], [ReqMet])
VALUES
		(N'5118604e-9f46-414a-acdc-a00102db3150', 1, 1),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 2, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 3, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 4, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 5, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 6, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 7, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 8, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 9, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 10, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 11, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 12, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 13, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 14, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 15, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 16, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 17, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 18, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 19, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 20, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 21, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 22, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 23, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 24, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 25, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 26, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 27, 0),
		(N'5118604e-9f46-414a-acdc-a00102db3150', 28, 0)
GO