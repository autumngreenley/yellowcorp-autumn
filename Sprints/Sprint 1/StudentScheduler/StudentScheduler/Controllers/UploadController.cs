﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Data;
using System.Data.Entity;
using OfficeOpenXml;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using Excel = Microsoft.Office.Interop.Excel;
using StudentScheduler.DAL;
using StudentScheduler.Models;
using StudentScheduler.Models.ViewModels;
using System.Runtime.InteropServices;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace StudentScheduler.Controllers
{
    public class UploadController : Controller
    {
        private ApplicationUserManager _userManager;
        private SchedulerContext db = new SchedulerContext();
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(StudentScheduler.Models.Upload importExcel)
        {
            if (ModelState.IsValid)
            {
                string path = Server.MapPath("~/Content/" + importExcel.file.FileName);
                /*                if (System.IO.File.Exists(path))
                                    System.IO.File.Delete(path);
                                    */
                
                    importExcel.file.SaveAs(path);

                    Excel.Application application = new Excel.Application();
                    Excel.Workbook workbook = application.Workbooks.Open(path);
                    Excel.Worksheet worksheet = workbook.ActiveSheet;
                    Excel.Range range = worksheet.UsedRange;
                    List<Completion> comps = new List<Completion>();
                    for (int row = 2; row < range.Rows.Count + 1; row++)
                    {
                        Completion p = new Completion();
                        p.UserID = User.Identity.GetUserId();
                        p.Requirements = ((Excel.Range)range.Cells[row, 1]).Text;
                        p.Subtexts = ((Excel.Range)range.Cells[row, 2]).Text;
                        p.CreditsRequired = int.Parse(((Excel.Range)range.Cells[row, 3]).Text);
                        p.CreditsCompleted = int.Parse(((Excel.Range)range.Cells[row, 4]).Text);
                        p.ClassesTaken = ((Excel.Range)range.Cells[row, 5]).Text;
                        comps.Add(p);
                        db.Completions.Add(p);
                        db.SaveChanges();
                    }


                    Marshal.ReleaseComObject(range);
                    Marshal.ReleaseComObject(worksheet);

                    workbook.Close();
                    Marshal.ReleaseComObject(workbook);
                    application.Quit();
                    Marshal.ReleaseComObject(application);
                    System.IO.File.Delete(path);
            }
            return View();
        }
        public ActionResult Progress()
        {
            string userId = User.Identity.GetUserId();
            IQueryable<CompletionVM> comps = null;
            IQueryable<CompletionVM> toDo = db.Completions.Select(c => new CompletionVM
            {
                UserID = c.UserID,
                Requirements = c.Requirements,
                Subtexts = c.Subtexts,
                ClassesTaken = c.ClassesTaken,
                CreditsCompleted = c.CreditsCompleted,
                CreditsRequired = c.CreditsRequired
            }).Where(com => com.CreditsCompleted < com.CreditsRequired).Where(com =>com.UserID == userId);
            if(toDo.Count() > 0)
            {
                ViewBag.toggle = 1;
                ViewBag.result = "These are the requirements you have not met yet";
            }
            return View(toDo.ToList());
        }
    }
}