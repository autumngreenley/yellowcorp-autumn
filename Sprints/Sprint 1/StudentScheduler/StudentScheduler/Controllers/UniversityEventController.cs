﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using StudentScheduler.DAL.Repositories.Abstract;
using StudentScheduler.Models;
using StudentScheduler.Extensions;

namespace StudentScheduler.Controllers
{
    public class UniversityEventController : Controller
    {
        //private SchedulerContext db = new SchedulerContext();
        private ApplicationUserManager _userManager;

        private IUnitOfWork unitOfWork;

        public UniversityEventController(IUnitOfWork myUnitOfWork)
        {
            unitOfWork = myUnitOfWork;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: UniversityEvent
        public ActionResult Index()
        {
            //IQueryable<UniversityEvent> universityEvents = null;
            IEnumerable<UniversityEvent> universityEvents = null;
            if (User.IsInRole("Administrator"))
            {
                // Administrators can see Majors for all schools
                //universityEvents = db.UniversityEvents.Include(u => u.School);
                universityEvents = unitOfWork.UniversityEvents.GetAll();
            }
            else
            {
                // All other roles should only see the Majors for their associated school
                int schoolId = User.Identity.GetSchoolId();
                //universityEvents = db.UniversityEvents.Where(m => m.SchoolID == schoolId);
                universityEvents = unitOfWork.UniversityEvents.GetUniversityEventsForSchool(schoolId);
            }

            //return View(universityEvents.ToList());
            return View(universityEvents);
        }

        // GET: UniversityEvent/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //UniversityEvent universityEvent = db.UniversityEvents.Find(id);
            UniversityEvent universityEvent = unitOfWork.UniversityEvents.Get(id ?? 0);
            if (universityEvent == null)
            {
                return HttpNotFound();
            }
            return View(universityEvent);
        }

        // GET: UniversityEvent/Create
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Create()
        {
            //ViewBag.SchoolID = new SelectList(db.Schools, "ID", "Title");
            ViewBag.SchoolID = new SelectList(unitOfWork.Schools.GetAll(), "ID", "Title");
            return View();
        }

        // POST: UniversityEvent/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Create([Bind(Include = "ID,SchoolID,EventTitle,EventDescription,EventDays,EventStartTime,EventEndTime")] UniversityEvent universityEvent)
        {
            if (ModelState.IsValid)
            {
                //db.UniversityEvents.Add(universityEvent);
                //db.SaveChanges();
                unitOfWork.UniversityEvents.Add(universityEvent);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            //ViewBag.SchoolID = new SelectList(db.Schools, "ID", "Title", universityEvent.SchoolID);
            ViewBag.SchoolID = new SelectList(unitOfWork.Schools.GetAll(), "ID", "Title");
            return View(universityEvent);
        }

        // GET: UniversityEvent/Edit/5
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //UniversityEvent universityEvent = db.UniversityEvents.Find(id);
            UniversityEvent universityEvent = unitOfWork.UniversityEvents.Get(id ?? 0);
            if (universityEvent == null)
            {
                return HttpNotFound();
            }
            //ViewBag.SchoolID = new SelectList(db.Schools, "ID", "Title", universityEvent.SchoolID);
            ViewBag.SchoolID = new SelectList(unitOfWork.Schools.GetAll(), "ID", "Title");
            return View(universityEvent);
        }

        // POST: UniversityEvent/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Edit([Bind(Include = "ID,SchoolID,EventTitle,EventDescription,EventDays,EventStartTime,EventEndTime")] UniversityEvent universityEvent)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(universityEvent).State = EntityState.Modified;
                //db.SaveChanges();
                unitOfWork.UniversityEvents.Update(universityEvent);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            //ViewBag.SchoolID = new SelectList(db.Schools, "ID", "Title", universityEvent.SchoolID);
            ViewBag.SchoolID = new SelectList(unitOfWork.Schools.GetAll(), "ID", "Title");
            return View(universityEvent);
        }

        // GET: UniversityEvent/Delete/5
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //UniversityEvent universityEvent = db.UniversityEvents.Find(id);
            UniversityEvent universityEvent = unitOfWork.UniversityEvents.Get(id ?? 0);
            if (universityEvent == null)
            {
                return HttpNotFound();
            }
            return View(universityEvent);
        }

        // POST: UniversityEvent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Staff")]
        public ActionResult DeleteConfirmed(int id)
        {
            //UniversityEvent universityEvent = db.UniversityEvents.Find(id);
            //db.UniversityEvents.Remove(universityEvent);
            //db.SaveChanges();
            UniversityEvent universityEvent = unitOfWork.UniversityEvents.Get(id);
            unitOfWork.UniversityEvents.Remove(universityEvent);
            unitOfWork.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
