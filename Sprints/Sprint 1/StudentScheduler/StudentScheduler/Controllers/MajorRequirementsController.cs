﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudentScheduler.DAL;
using StudentScheduler.Models;

namespace StudentScheduler.Controllers
{
    public class MajorRequirementsController : Controller
    {
        private SchedulerContext db = new SchedulerContext();

        // GET: MajorRequirements
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                ViewBag.MajorID =  null;
                ViewBag.Title = "Requirements for all majors:";
                IQueryable<MajorRequirement> majorRequirements = db.MajorRequirements.Include(m => m.Major).Include(m => m.Requirement);
                return View(majorRequirements.ToList());
            }
            else
            { 
                var major = db.Majors.Find(id);
                if (major == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    ViewBag.MajorID = major.ID;
                    ViewBag.Title = "Requirements for " + major.School.Title + " | " + major.Title + ":";
                    IQueryable<MajorRequirement> majorRequirements = db.MajorRequirements.Where(m => m.MajorID == id).Include(m => m.Major).Include(m => m.Requirement);
                    return View(majorRequirements.ToList());
                }
            }
        }

        // GET: MajorRequirements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MajorRequirement majorRequirement = db.MajorRequirements.Find(id);
            if (majorRequirement == null)
            {
                return HttpNotFound();
            }
            return View(majorRequirement);
        }

        // GET: MajorRequirements/Create
        public ActionResult Create(int? id)
        {
            if (id != null)
            {
                var major = db.Majors.Find(id);
                if (major == null)
                {
                    return HttpNotFound();
                }
            }

            // Get list of requirements already used for the major (to avoid duplicates)
            var usedReqs = db.MajorRequirements.Where(mr => mr.MajorID == id).Select(mr => mr.ReqID);
            
            // Get a SelectList of Requirements NOT already in use for the major
            ViewBag.ReqID = new SelectList(db.Requirements.Where(r => !usedReqs.Contains(r.ID)), "ID", "RequirementTitle"); ;

            // Filter Major SelectList to only be the major being created for
            ViewBag.MajorID = new SelectList(db.Majors.Where(m => m.ID == id), "ID", "Title");

            // Set a separate Viewbag item for redirect when posting back
            ViewBag.SelectedMajorID = id;

            return View();
        }

        // POST: MajorRequirements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,MajorID,ReqID")] MajorRequirement majorRequirement)
        {
            if (ModelState.IsValid)
            {
                db.MajorRequirements.Add(majorRequirement);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = majorRequirement.MajorID });
            }

            ViewBag.MajorID = new SelectList(db.Majors, "ID", "Title", majorRequirement.MajorID);
            ViewBag.ReqID = new SelectList(db.Requirements, "ID", "RequirementTitle", majorRequirement.ReqID);
            return View(majorRequirement);
        }

        // GET: MajorRequirements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                var major = db.Majors.Find(id);
                if (major == null)
                {
                    return HttpNotFound();
                }
            }

            // Get list of requirements already used for the major (to avoid duplicates)
            var usedReqs = db.MajorRequirements.Where(mr => mr.MajorID == id).Select(mr => mr.ReqID);

            // Get a SelectList of Requirements NOT already in use for the major
            ViewBag.ReqID = new SelectList(db.Requirements.Where(r => !usedReqs.Contains(r.ID)), "ID", "RequirementTitle"); ;

            // Filter Major SelectList to only be the major being created for
            ViewBag.MajorID = new SelectList(db.Majors.Where(m => m.ID == id), "ID", "Title");



            // ===========================================

            
            MajorRequirement majorRequirement = db.MajorRequirements.Find(id);
            if (majorRequirement == null)
            {
                return HttpNotFound();
            }
            //ViewBag.MajorID = new SelectList(db.Majors, "ID", "Title", majorRequirement.MajorID);
            //ViewBag.ReqID = new SelectList(db.Requirements, "ID", "RequirementTitle", majorRequirement.ReqID);
            return View(majorRequirement);
        }

        // POST: MajorRequirements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,MajorID,ReqID")] MajorRequirement majorRequirement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(majorRequirement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = majorRequirement.MajorID });
            }
            ViewBag.MajorID = new SelectList(db.Majors, "ID", "Title", majorRequirement.MajorID);
            ViewBag.ReqID = new SelectList(db.Requirements, "ID", "RequirementTitle", majorRequirement.ReqID);
            return View(majorRequirement);
        }

        // GET: MajorRequirements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MajorRequirement majorRequirement = db.MajorRequirements.Find(id);
            if (majorRequirement == null)
            {
                return HttpNotFound();
            }
            return View(majorRequirement);
        }

        // POST: MajorRequirements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MajorRequirement majorRequirement = db.MajorRequirements.Find(id);
            db.MajorRequirements.Remove(majorRequirement);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = majorRequirement.MajorID });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
