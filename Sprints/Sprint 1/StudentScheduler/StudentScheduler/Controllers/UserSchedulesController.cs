﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using StudentScheduler.DAL;
using StudentScheduler.Models;
namespace StudentScheduler.Controllers
{
    public class UserSchedulesController : Controller
    {
        private SchedulerContext db = new SchedulerContext();

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: UserSchedules
        public ActionResult Index()
        {
            IQueryable<UserSchedule> schedules = null;
            if (User.IsInRole("Student"))
            {
                string userId = User.Identity.GetUserId();
                schedules = db.UserSchedules.Where(m => m.UserID == userId);
            }
            else
            {
                schedules = db.UserSchedules.Where(m => m.UserID == "none");
            }
                //var userSchedules = db.UserSchedules.Include(u => u.AspNetUser);
            return View(schedules.ToList());
        }

        // GET: UserSchedules/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSchedule userSchedule = db.UserSchedules.Find(id);
            if (userSchedule == null)
            {
                return HttpNotFound();
            }
            return View(userSchedule);
        }

        // GET: UserSchedules/Create
        [Authorize(Roles = "Student")]
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email");
            return View();
        }

        // POST: UserSchedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Student")]
        public ActionResult Create([Bind(Include = "ID,UserID,ActivityTitle,ActivityDescription,ActivityDays,ActivityStartTime,ActivityEndTime")] UserSchedule userSchedule)
        {
            if (ModelState.IsValid)
            {
                userSchedule.UserID = User.Identity.GetUserId();
                db.UserSchedules.Add(userSchedule);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", userSchedule.UserID);
            return View(userSchedule);
        }

        // GET: UserSchedules/Edit/5
        [Authorize(Roles = "Student")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSchedule userSchedule = db.UserSchedules.Find(id);
            if (userSchedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", userSchedule.UserID);
            return View(userSchedule);
        }

        // POST: UserSchedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Student")]
        public ActionResult Edit([Bind(Include = "ID,UserID,ActivityTitle,ActivityDescription,ActivityDays,ActivityStartTime,ActivityEndTime")] UserSchedule userSchedule)
        {
            if (ModelState.IsValid)
            {
                userSchedule.UserID = User.Identity.GetUserId();
                db.Entry(userSchedule).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", userSchedule.UserID);
            return View(userSchedule);
        }

        // GET: UserSchedules/Delete/5
        [Authorize(Roles = "Student")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSchedule userSchedule = db.UserSchedules.Find(id);
            if (userSchedule == null)
            {
                return HttpNotFound();
            }
            return View(userSchedule);
        }

        // POST: UserSchedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Student")]
        public ActionResult DeleteConfirmed(int id)
        {
            UserSchedule userSchedule = db.UserSchedules.Find(id);
            db.UserSchedules.Remove(userSchedule);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
