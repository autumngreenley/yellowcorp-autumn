﻿using Ninject;
using StudentScheduler.DAL.Repositories.Abstract;
using StudentScheduler.DAL.Repositories.Concrete;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace StudentScheduler.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel myKernel)
        {
            kernel = myKernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<IMajorRepository>().To<MajorRepository>();
            kernel.Bind<ISchoolRepository>().To<SchoolRepository>();
            kernel.Bind<IUniversityEventRepository>().To<UniversityEventRepository>();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}