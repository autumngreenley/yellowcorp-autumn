﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Security.Principal;
using System.Web.Mvc;
using StudentScheduler.Controllers;
using StudentScheduler.DAL.Repositories.Abstract;
using StudentScheduler.Models;

namespace UnitTestStudentScheduler.ControllerTests
{
    [TestClass]
    public class MajorsController_Test
    {
        private Mock<IUnitOfWork> mockUnitOfWork = new Mock<IUnitOfWork>();

        //Constructor
        public MajorsController_Test()
        {
            School[] schools = new School[]
            {
                new School {ID = 1, Title="MOCK Western Oregon University"},
                new School {ID = 2, Title="MOCK Oregon State University"},
                new School {ID = 1, Title="MOCK University of Oregon"}
            };


            Major[] majors = new Major[]
            {
                new Major {ID = 1, Title="MOCK Computer Science", SchoolID = 1},
                new Major {ID = 2, Title="MOCK Information Systems", SchoolID = 1},
                new Major {ID = 3, Title="MOCK Visual Communications Design", SchoolID = 1},
                new Major {ID = 4, Title="MOCK Computer Science", SchoolID = 2}
            };

            /*
            Object[] users = new Object[]
            {
                new { Email = "student@test.com", UserName = "student@test.com", UserFullName = "Joe Studential", SchoolId = 1, Roles = new String[] {"Student"}},
                new { Email = "staff@test.com", UserName = "staff@test.com", UserFullName = "Ann Staffer", SchoolId = 1, Roles = new String[] {"Staff"}},
                new { Email = "administrator@test.com", UserName = "administrator@test.com", UserFullName = "Elizabeth Adminieweather", SchoolId = 0, Roles = new String[] {"Administrator"}}
            };
            */
            
            mockUnitOfWork.Setup(s => s.Schools.GetAll()).Returns(schools);
            mockUnitOfWork.Setup(m => m.Schools.Get(It.IsAny<int>())).Returns((int x) => { return Array.Find(schools, s => s.ID == x); });
            mockUnitOfWork.Setup(m => m.Majors.GetAll()).Returns(majors);
            mockUnitOfWork.Setup(m => m.Majors.Get(It.IsAny<int>())).Returns((int x) => { return Array.Find(majors, m => m.ID == x); });
            mockUnitOfWork.Setup(m => m.Majors.GetMajorsForSchool(It.IsAny<int>())).Returns((int s) => { return Array.FindAll(majors, m => m.SchoolID == s); }); 
        }

        [TestMethod]
        public void Edit_WithSchoolID_ReturnsCorrectItem()
        {
            // Arrange
            // mocked data is created at the constructor
            MajorsController controller = new MajorsController(mockUnitOfWork.Object);
            

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;
            var major = (Major)result.ViewData.Model;

            // Assert
            Assert.AreEqual("MOCK Computer Science", major.Title);
        }

        [TestMethod]
        public void Index_NonAdministrator_ReturnsMajorsForSchoolID()
        {
            // Arrange
            // mocked data is created at the constructor
            MajorsController controller = new MajorsController(mockUnitOfWork.Object);

            Mock<ControllerContext> mockControllerContext = new Mock<ControllerContext>();
            Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();
            mockPrincipal.Setup(p => p.IsInRole("Administrator")).Returns(false);
            mockControllerContext.SetupGet(x => x.HttpContext.User).Returns(mockPrincipal.Object);
            controller.ControllerContext = mockControllerContext.Object;

            // CANT SETUP THIS VALUE BECAUSE IT IS FROM EXTENSION CLASS TO IDENTITY (STATIC)
            //mockPrincipal.Setup(p => p.Identity.GetSchoolId()).Returns(1);
            //mockControllerContext.Setup(x => x.HttpContext.User.Identity.GetSchoolId()).Returns(1);
            controller.userSchoolId = 1;

            // Act
            ViewResult result = controller.Index() as ViewResult;
            Major[] majors = (Major[])result.ViewData.Model;

            // Assert
            Assert.AreEqual(3, majors.Length);
            foreach (Major major in majors)
            {
                Assert.AreEqual(1, major.SchoolID);
            }
        }

        [TestMethod]
        public void Index_Administrator_ReturnsMajorsForAllSchools()
        {
            // Arrange
            // mocked data is created at the constructor
            MajorsController controller = new MajorsController(mockUnitOfWork.Object);

            Mock<ControllerContext> mockControllerContext = new Mock<ControllerContext>();
            Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();
            mockPrincipal.Setup(p => p.IsInRole("Administrator")).Returns(true);
            mockPrincipal.Setup(p => p.Identity.IsAuthenticated).Returns(true);
            mockControllerContext.SetupGet(x => x.HttpContext.User).Returns(mockPrincipal.Object);
            controller.ControllerContext = mockControllerContext.Object;

            // Administrator is not associated with any school - set to zero
            controller.userSchoolId = 0;

            // Act
            ViewResult result = controller.Index() as ViewResult;
            Major[] majors = (Major[])result.ViewData.Model;

            // Assert
            Assert.AreEqual(4, majors.Length);
        }

        [TestMethod]
        public void Index_NonAuthenticatedUser_ReturnsMajorsForAllSchools()
        {
            // Arrange
            // mocked data is created at the constructor
            MajorsController controller = new MajorsController(mockUnitOfWork.Object);

            Mock<ControllerContext> mockControllerContext = new Mock<ControllerContext>();
            Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();
            mockPrincipal.Setup(p => p.Identity.IsAuthenticated).Returns(false);
            mockControllerContext.SetupGet(x => x.HttpContext.User).Returns(mockPrincipal.Object);
            controller.ControllerContext = mockControllerContext.Object;

            // Administrator is not associated with any school - set to zero
            controller.userSchoolId = 0;

            // Act
            ViewResult result = controller.Index() as ViewResult;
            Major[] majors = (Major[])result.ViewData.Model;

            // Assert
            Assert.AreEqual(4, majors.Length);
        }
    }
}



// SOME SAMPLES FOR FUTURE USE
//articlesRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask)