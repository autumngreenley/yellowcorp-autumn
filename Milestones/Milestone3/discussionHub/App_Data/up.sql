CREATE TABLE Users
(
	userID 						INT IDENTITY(1,1)	NOT NULL,
	userName 					VARCHAR(100)		NOT NULL,
	userDOB						DATE				NOT NULL,
	userEmail					VARCHAR(100)		NOT NULL,
	userCountry					VARCHAR(75)			NOT NULL,
	userStateProvince			VARCHAR(100)		NOT NULL,
	userPoliticalParty			VARCHAR(100)		NOT NULL,
	userPassword				VARCHAR(25)			NOT NULL, --This would need to change for real passwords with storing the hashes of the passwords.
	CONSTRAINT PK_Users_userID PRIMARY KEY CLUSTERED (userID)
);
GO

CREATE TABLE Discussions
(
	discussionID				INT IDENTITY(1,1)	NOT NULL,
	discussionUserID			INT					NOT NULL,
	discussionTitle				VARCHAR(120)		NOT NULL,
	discussionDateTime			DATETIME			NOT NULL,
	discussionActive			BIT					NOT NULL	DEFAULT 1,
	CONSTRAINT PK_Discussions_discussionID PRIMARY KEY CLUSTERED (discussionID),
	CONSTRAINT FK_Discussions_userID FOREIGN KEY (discussionUserID)
		REFERENCES Users(userID)
);
GO

CREATE TABLE Comments
(
	commentID					INT IDENTITY(1,1)	NOT NULL,
	commentDiscussionID			INT					NOT NULL,
	commentUserID				INT					NOT NULL,
	commentDateTime				DATETIME			NOT NULL,
	commentContents				VARCHAR(MAX)		NOT NULL,
	CONSTRAINT PK_Comments_commentID PRIMARY KEY CLUSTERED (commentID),
	CONSTRAINT FK_Comments_userID FOREIGN KEY (commentUserID)
		REFERENCES Users(userID),
	CONSTRAINT FK_Comments_discussionID FOREIGN KEY (commentDiscussionID)
		REFERENCES Discussions(discussionID)
);
GO

INSERT INTO Users
(
	userName,
	userCountry,
	userStateProvince,
	userPoliticalParty,
	userEmail,
	userDOB,
	userPassword
)
VALUES
('JD12345', 'United States', 'Oregon', 'Green Party', 'jdawg@gmail.com', '1998-04-18', 'jdawgrulez'),
('JaneD1992', 'United States', 'Washington', 'Democract', 'catlover1992@hotmail.com', '1992-11-29', 'tabbys4life'),
('spooderman', 'United States', 'New York', 'Unaffiliated', 'milesmorales@gmail.com', '2003-07-14', 'spidermanspiderman'),
('firealarm1111', 'Australia', 'New South Wales', 'Australian Labor Party', 'sydneyFireOpera@aol.com', '1996-09-03', 'Ih8dingos'),
('dry Erase Marker', 'Japan', 'Tokyo', 'Social Democratic Party', 'dem@gmail.com', '2001-01-25', '1234567890')
GO

INSERT INTO Discussions
(
	discussionTitle,
	discussionDateTime,
	discussionUserID,
	discussionActive
)
VALUES
('Flights delayed at major US airports because of staffing, FFA says', '2019-01-23 14:45:21', 1, 1),
('Police officers fall through ice during tense rescue', '2019-01-25 22:21:50', 1, 1),
('Rescuers were combing the woods when they heard the missing 3-year-old boy call out for his mom', '2019-01-25 05:43:11', 3, 1),
('Private electrical system caused fire that killed 22, California says', '2019-01-25 11:22:33', 5, 1),
('A suspect killed five people inside a SunTrust bank branch in Florida. Just one employee escaped.', '2019-01-24 23:59:59', 3, 1)
GO

INSERT INTO Comments
(
	commentContents,
	commentDateTime,
	commentUserID,
	commentDiscussionID
)
VALUES
('I really wish this shutdown would end.', '2019-01-23 16:21:45', 4, 1),
('It was worring that the rescuers were in danger but I am glad that they are alright. What happened?', '2019-01-26 05:41:52', 4, 1),
('These kind of tradgedies should be stopped!', '2019-01-25 00:23:15', 2, 5),
('Shouldnt they be considered esential personel!', '2019-01-25 02:05:11', 2, 1),
('Im glad the 3 year old is safe, t&p', '2019-01-25 14:22:33', 3, 3)
GO