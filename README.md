# Welcome to Yellow Umbrella Corp

## Description:
For university students who are looking to elevate their schedule of classes next term, Yellow Umbrella Corp has a website that lets you schedule your next term by cross referencing the classes you still need to take, 
and the classes available next term. All while keeping in mind your personal schedule that you will provide. The site will allow users to set certain criteria/filters, for example,
limit amount of credits they want to take during the term, time of day they prefer to be on campus, and location of classes. The website determines the number of classes the user still needs to take,
by having a database of users’ graduation requirement information. Once all needed information is gathered, the website will show the user different schedules that fulfill the filters and obligations the user provided.


## Guidelines:
* Always make sure to pull development branch before starting a feature branch
* Feature branch name style: ft_numberOfPBI_yourName (ex: ft_104_bob)
* Primary key for tables will use “ID” style, not “ProdcutID”
* Write XML comments for all public methods.
* Primary language will be C


## Team Members: 
* Joshua
* Schaefer
* Autumn
* Launia


## Software Construction:
* Agile Software Construction Process
* Disciplined Agile Delivery Lifecycle 


## Team Properties:
### Rules: 
What happens in Yellow Umbrella Corp., stays in Yellow Umbrella Corp. 
### Song: 
It's Not Unusual - Tom Jones
### Cheer: 
Yellow Umbrella Cheer (Hillshire Farms Tune)
### Dance Move: 
Carlton Dance
### Mascot: 
Umbrella with eyes


## Tools:
* Bitbucket
* Git
* Internet
* WOU Schedule of classes
* Students’ graduation requirements
* Azure
* Continuous deployment
* Visual Studio v
* Discord
* Google Docs
* 


## Installations:
* Bootstrap 4
* Entity Framework 6
* jQuery